::
:: Build the XAMPP runtime folder for the given version number
::

:: Make temp folder if it does not exist
if not exist "temp" (
    mkdir temp
)

:: Ensure no existing xampp
if exist C:\xampp (
    echo.
    echo WARNING - C:\xampp folder already exists. Please press Ctrl + c to exit.
    pause
) else (
    curl -L https://www.apachefriends.org/xampp-files/7.4.28/xampp-windows-x64-7.4.28-0-VC15-installer.exe --output %cd%/temp/xampp-setup.exe
    start /w %cd%/temp\xampp-setup.exe --disable-components xampp_filezilla,xampp_mercury,xampp_tomcat,xampp_perl,xampp_phpmyadmin,xampp_webalizer,xampp_sendmail --mode unattended --launchapps 0
    move c:\xampp .\7.4.28
)
rd /s /q temp

:: Bundle pre-built xdebug
curl -L https://xdebug.org/files/php_xdebug-3.1.1-7.4-vc15-x86_64.dll --output %cd%/7.4.28/php/ext/php_xdebug.dll
curl -L https://raw.githubusercontent.com/xdebug/xdebug/master/LICENSE --output %cd%/7.4.28/licenses/xdebug.txt

:: Purge unnecessary folders
rd /s /q 7.4.28\anonymous
rd /s /q 7.4.28\img
rd /s /q 7.4.28\install
rd /s /q 7.4.28\mailtodisk
rd /s /q 7.4.28\mailoutput
rd /s /q 7.4.28\src
rd /s /q 7.4.28\webdav

:: Purge unnecessary files
del /f /q 7.4.28\apache_start.bat
del /f /q 7.4.28\apache_stop.bat
del /f /q 7.4.28\catalina_service.bat
del /f /q 7.4.28\catalina_start.bat
del /f /q 7.4.28\catalina_stop.bat
del /f /q 7.4.28\ctlscript.bat
del /f /q 7.4.28\filezilla_setup.bat
del /f /q 7.4.28\filezilla_start.bat
del /f /q 7.4.28\filezilla_stop.bat
del /f /q 7.4.28\killprocess.bat
del /f /q 7.4.28\mercury_start.bat
del /f /q 7.4.28\mercury_stop.bat
del /f /q 7.4.28\mysql_start.bat
del /f /q 7.4.28\mysql_stop.bat
del /f /q 7.4.28\passwords.txt
del /f /q 7.4.28\properties.ini
del /f /q 7.4.28\readme_en.txt
del /f /q 7.4.28\readme_de.txt
del /f /q 7.4.28\service.exe
del /f /q 7.4.28\setup_xampp.bat
del /f /q 7.4.28\test_php.bat
del /f /q 7.4.28\uninstall.dat
del /f /q 7.4.28\uninstall.exe
del /f /q 7.4.28\xampp_shell.bat
del /f /q 7.4.28\xampp-control.exe
del /f /q 7.4.28\xampp-control.ini
del /f /q 7.4.28\xampp_start.exe
del /f /q 7.4.28\xampp_stop.exe

:: Purge zero-byte files (git doesn't like 'em)
del /f /q 7.4.28\apache\logs\access.log
del /f /q 7.4.28\apache\logs\ssl_request.log
del /f /q 7.4.28\mysql\backup\multi-master.info
del /f /q 7.4.28\mysql\backup\mysql\general_log.CSV
del /f /q 7.4.28\mysql\backup\mysql\slow_log.CSV
del /f /q 7.4.28\mysql\data\multi-master.info
del /f /q 7.4.28\mysql\data\mysql\general_log.CSV
del /f /q 7.4.28\mysql\data\mysql\slow_log.CSV
del /f /q 7.4.28\php\pear\.depdblock
del /f /q 7.4.28\php\pear\.lock

:: Purge empty directories (git doesn't like 'em either)
rd /s /q 7.4.28\htdocs\webalizer
rd /s /q 7.4.28\php\cfg
rd /s /q 7.4.28\php\man
rd /s /q 7.4.28\php\pear\.registry\.channel.doc.php.net
rd /s /q 7.4.28\php\pear\.registry\.channel.pecl.php.net
rd /s /q 7.4.28\php\pear\.registry\.channel.__uri
rd /s /q 7.4.28\php\tmp
rd /s /q 7.4.28\php\www

:: Include Visual C++ Runtime
copy %SystemRoot%\System32\vcruntime140.dll 7.4.28\php

To build the latest runtime

1. Ensure you have latest Visual C++ 2015 Runtimes installed (https://www.microsoft.com/en-us/download/details.aspx?id=53587)
2. Start a command line prompt (cmd.exe)
3. Change directory to project (i.e. c:\Users\jsmith\Documents\xampp_win_x64)
4. Execute build.bat (be sure to answer any UAC prompts)
